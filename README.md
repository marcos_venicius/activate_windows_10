# ACTIVATE WINDOWS 10

## EDITIONS

- HOME
- HOME N
- PROFESSIONAL
- PROFESSIONAL N

## HOW TO USE?

**RUN COMMAND PROMPT AS ADMINISTRATOR**

```batch
activate_windows_10.cmd <edition>
```

### EDITION OPTIONS

```bash
"home" | "home_n" | "professional" | "professional_n"
```
