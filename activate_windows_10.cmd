@echo off

:: BY @marcos-dev-web - https://marcosdevweb.herokuapp.com - @marcos.dev.web

set system_edition=%1

if [%system_edition%] EQU [] (
  echo [!] MISSING SYSTEM EDITION
  exit /b
)

if [%system_edition%] EQU [home] (
  echo ACTIVATING WINDOWS HOME
  echo.
  slmgr /ipk TX9XD-98N7V-6WMQ6-BX7FG-H8Q99
  slmgr /skms kms8.msguides.com
  set success=1
)

if [%system_edition%] EQU [home_n] (
  echo ACTIVATING WINDOWS HOME N
  echo.
  slmgr /ipk 3KHY7-WNT83-DGQKR-F7HPR-844BM
  slmgr /skms kms8.msguides.com
  set success=1
)

if [%system_edition%] EQU [professional] (
  echo ACTIVATING WINDOWS PROFESSIONAL
  echo.
  slmgr /ipk W269N-WFGWX-YVC9B-4J6C9-T83GX
  slmgr /skms kms8.msguides.com
  set success=1
)

if [%system_edition%] EQU [professional_n] (
  echo ACTIVATING WINDOWS PROFESSIONAL N
  echo.
  slmgr /ipk MH37W-N47XK-V7XM9-C7227-GCQG9
  slmgr /skms kms8.msguides.com
  set success=1
)

if [%success%] EQU [1] ECHO WINDOWS ACTIVATED
